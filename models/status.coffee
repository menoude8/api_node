mongoose = require 'mongoose'
Schema = mongoose.Schema

#Mongoose Schema
Status = new Schema
	_creator:
		type: Schema.ObjectId
		ref: 'User'
	content:
		type: String
		default: ''
	comments:[
	 	from:
	 		type: Schema.ObjectId
	 		ref: 'User'
	 	field:
	 		type: Schema.ObjectId
	 		ref: 'Comment'
 	]

module.exports = Status = mongoose.model 'Status', Status