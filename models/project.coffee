mongoose = require 'mongoose'
Schema = mongoose.Schema

#Mongoose Schema
Project = new Schema
 _creator:
 	type: Schema.ObjectId
 	ref: 'User'
 illustration:
 	type: 'String'
 	default: 'client/public/assets/images/default-project.png'
 date:
 	type: Date
 	default: Date.now
 title:
 	type: String
 	default: 'Untitled'
 resum:
 	type: String
 	default: 'no resum avalaible'
 description:
 	type: String
 	default: 'No description avalaible'
 amount:
 	type: String
 	default: "0€"
 receipts: [
 	type: Number
 ]
 tags: [
 	type: String
 ]
 collaborators: [
 	collaborator:
 		type: Schema.ObjectId
 		ref: 'User'
 	amount:
 		type: Number
 ]
 likers:[
 	type: String
 ]
 comments:[
 	from:
 		type: Schema.ObjectId
 		ref: 'User'
 	field:
 		type: Schema.ObjectId
 		ref: 'Comment'
 ]
 room:
 	to_do:[
 		type: Schema.ObjectId
 		ref: 'Task'
 	]
 	doing:[
 		type: Schema.ObjectId
 		ref: 'Task'
 	]
 	done:[
 		type: Schema.ObjectId
 		ref: 'Task'
 	]
 	members:[
 		type: Schema.ObjectId
 		ref: 'User'
 	]
module.exports = Project = mongoose.model 'Project', Project