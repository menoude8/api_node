mongoose = require 'mongoose'
Schema = mongoose.Schema

#Mongoose Schema
Notification = new Schema
	from:
		username: String
		avatar: String
	for:
		type: Schema.ObjectId
		ref: 'User'
	date:
		type: Date
		default: Date.now
	read:
		type: Boolean
		default: false
	data:
		type: String
		default: "...bloom?"
	deleted:
		type: Boolean
		default: false
module.exports = Notification = mongoose.model 'Notification', Notification