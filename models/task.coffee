mongoose = require 'mongoose'
Schema = mongoose.Schema

#Mongoose Schema
Task = new Schema
	_creator:
		type: Schema.ObjectId
		ref: 'User'
	content:
		type: String
		default: ''
	assign: [
		type: Schema.ObjectId
		ref: 'User'
	]
	description: String
	comments:[
	 	from:
	 		type: Schema.ObjectId
	 		ref: 'User'
	 	field:
	 		type: Schema.ObjectId
	 		ref: 'Comment'
 	]

module.exports = Task = mongoose.model 'Task', Task