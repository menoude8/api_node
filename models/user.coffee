mongoose = require 'mongoose'
Schema = mongoose.Schema
crypto = require('crypto')
mailer = require '../libs/emailer'
config = require("../config")
bcrypt = require 'bcrypt'

# Mongoose schema

User = new Schema
  username:
    type: String
    default: 'Undefined'
  working_at:
    type: String
    default: 'Preziew'
  zip_code:
    type: String
    default: '00000'
  predilection:
    type: String
    default: "None"
  status:
    type:String
    default: 'What a fun project'
  master_domain:
    type: String
    default: 'Technology'
  Bio:
    type: String
    default: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus, eveniet, dolores, sit omnis facere odio rerum laudantium temporibus nam nulla ratione quam quae sint illo similique quis nesciunt repudiandae quisquam.'
  contact:
    type: String
    default: '752033'
  website:
    type: String
    default: 'www.3project.com'
  avatar:
    type: String
    default: "client/public/assets/images/silhouette.jpg"
  cover:
    type: String
    default: null
  email:
    type: String
    unique: yes
  password:
    type: String
  activation:
    type: Boolean
  activation_key:
    type: String
  forgot_password_key:
    type: String
  token:
    type: String
  projects: [
    type: Schema.ObjectId
    ref: 'Project'
  ]
  followers:[
    type: Schema.ObjectId
    ref: 'User'
  ]
  followings:[
    type: Schema.ObjectId
    ref: 'User'
  ]
  consulters:[
    type: Schema.ObjectId
    ref: 'User'
  ]
  notifications:[
    type: Schema.ObjectId
    ref: 'Notification'
  ]
  invitations: [
    type: Schema.ObjectId
    ref: 'Project'
  ]
  activity: [
    to:
      type: String
    reference:
      type: String
    projectref:
      type: Schema.ObjectId
      ref: 'Project'
    Date:
      type: Date
      default: Date.now
  ]

  #generate token function
User.statics.generateToken = ->
    crypto.randomBytes(16).toString('hex')

User.statics.getcurrentuser = (currentusername, cb)->
  User.findOne
    email: currentusername
  ,(err, user)->
    cb user
    if err
      return handleError
    return user   

User.methods.comparePassword = (candidatePassword, cb)->
  bcrypt.compare candidatePassword, @password, (err, isMatch)->
    if err
      return cb err
    cb null, isMatch


User.methods.sendEmailActivation = (options, data) ->
  options.template = "activate"
  emailer = new mailer options, data
  emailer.send (err, result) ->
    if err
      console.log err
    else
      console.log "Message sent: " + result.message



module.exports = User = mongoose.model 'User', User


