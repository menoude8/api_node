###
Module dependencies.
var multer  = require('multer')

app.use(multer({ dest: './uploads/'}))
###
express = require("express")
path = require("path")
mongoose     = require 'mongoose'
config = require("./config")
session = require('express-session')
RedisStore   = require('connect-redis')(session)
redis = require("heroku-redis-client")
multer = require("multer")
mongoose.connect config.databases.mongo_uri
app = express()
server = require('http').Server(app)
##MemStore = express.session.MemoryStore;

app.set "port", process.env.PORT or 3000
app.set 'views', "#{__dirname}/views"
app.set "view engine", "jade"
app.use(multer({dest: './client/public/assets/uploads'}))
app.use express.favicon()
app.use express.logger("dev")
app.use express.json()
app.use express.urlencoded()
app.use express.methodOverride()
app.use express.cookieParser()
app.use session
    store   : new RedisStore client: redis.createClient(config.databases.redis)
    secret  : config.session.secret
    resave	: true
    saveUninitialized	: true
app.use require('connect-coffee-script')
    src     : "#{__dirname}/client/assets/coffee"
    dest    : "#{__dirname}/client/public/js"
    prefix  : '/js'

app.use express.static "#{__dirname}/client/public"
app.use app.router

app.use express.errorHandler()  if "development" is app.get("env")
require("./routes") app
io = require('socket.io') server
require('./routes/sockets') io

server.listen app.get("port"), ->
  console.log "Express server listening on port " + app.get("port")
