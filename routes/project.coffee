validator = require '../libs/validator'
config = require("../config")
User = require '../models/user'
Project = require '../models/project'
Comment = require '../models/comment'
mailer = require '../libs/emailer'

module.exports = (app) ->

	app.post '/project', (req, res, callback) ->
	    User.findOne
	      email: req.session.email
	     ,(err, user)->
	     	##tagarray = req.body.tags.split(" ")
	     	if user
	     		tagsupper = req.body.tags.toUpperCase()
	     		tagarray = tagsupper.split(" ")
	     		project = new Project
	     			_creator: user._id
	     			title: req.body.title
	     			resum: req.body.resum
	     			amount: req.body.amount
	     			tags: tagarray
	     			##tags: tagarray
	     		project.save (err, project) ->
	     			if err
	     				return res.send err, 400
	     			user.activity = []
	     			user.activity.push(
	     				to: project.title
	     				reference: "Created new project"
	     				projectref: project._id
	     			)
	     			user.projects.push project
	     			user.save (err)->
	     				if err
	     					return res.send err, 400
	     		res.send project, 200
	     	else
	     		res.send "Your session has been canceled", 400

    app.get '/get/projects', (req, res, callback) ->
    	User.findOne
    		email: req.session.email
    	,(err, user)->
    		if user
    			Project.find(
    				_creator: user._id
    			).sort(date: -1)
    			.exec (err, projects)->
    				if projects
    					return res.send projects, 200
    				else
    					return res.send err, 400
    		else
    			return res.send "Your session has been canceled"
    				

	app.get '/project/:id', (req, res, callback) ->
		if req.session.email
			Project.findOne(
				_id: req.params.id
			).populate('_creator')
			.populate('collaborators.collaborator')
			.populate('comments.from')
			.populate('comments.field')
			.exec (err, project)->
				if project
					res.send project, 200
				else
					res.send "Project not found or doesnt exist anymore", 400
		else
			return res.send "Authentification failed", 400

	app.post '/editproject/:id', (req, res, callback)->
		Project.findOne
			_id: req.params.id
		,(err, project)->
			if project
				project.title = req.body.title
				project.resum = req.body.resum
				project.amount = req.body.amount
				project.description = req.body.description
				project.save (err, project)->
					if err
						return res.send err, 400
					return res.send project, 200
			else
				return res.send err, 400

	app.post '/project/pictureupload/:id', (req, res, callback)->
		if req.session.email
			Project.findOne
				_id: req.params.id
			,(err, project)->
				if req.files.projectPhoto
					project.illustration = req.files.projectPhoto.path
					project.save (err)->
						if err
							return res.send err, 400
						res.redirect "/me/timeline"
				else
					return res.send "Must upload picture", 400
		else
			return res.send "No session available", 400

	app.get '/getprojecttags/:tags', (req, res, callback)->
		Project.find(
			tags: $in: [req.params.tags.toUpperCase()]
		).populate('_creator')
		.sort(date: -1)
		.exec (err, projects)->
			if err
				return res.send err, 400
			return res.send projects, 200

	app.get '/project/like/:projectid', (req, res, callback)->
		if req.session.email
			Project.findOne
				_id: req.params.projectid
			,(err, project)->
				if project
					project.likers.push req.session.email
					project.save (err)->
						if err
							return res.send err, 400
					return res.send project, 200
				else
					return res.send err, 400

	sendemail = (user, project, amount)->
		Project.findOne(
			_id: project._id
		).populate('_creator')
		.exec (err, project)->
			if err
				return res.send err, 400
			options = 
				to:
					email: project._creator.email
					subject: "NEW CONTRIBUTOR"
			data = 
				sender: user.username
				user: user
				to: project._creator.email
				projectname: project.title
				amount: amount
			console.log amount
			options.template = "contributor"
			emailer = new mailer options, data
			emailer.send (err, result) ->
				if err
					console.log err
				else
					console.log "Message sent: " + result.message

	app.post '/project/promote/:projectid', (req, res, callback)->
		if req.session.email
			Project.findOne
				_id: req.params.projectid
			,(err, project)->
				if err
					return res.send err, 400
				User.findOne
					email: req.session.email
				,(err, user)->
					if err
						return res.send err, 400
					project.collaborators.push(
						collaborator: user._id
						amount: req.body.amount
					)
					project.save (err, project)->
						if err
							return res.send err, 400
						sendemail(user, project, req.body.amount)
						return res.send project, 200
	app.post '/commentproject/:id', (req, res, callback)->
		if req.session.email
			User.findOne
				email: req.session.email
			,(err, user)->
				if err
					return res.send err, 400
				Project.findOne
					_id: req.params.id
				,(err, project)->
					return res.send err, 400 if err
					comment = new Comment
						type: 'Project'
						message: req.body.comment
					comment.save (err, comment)->
						return res.send err, 400 if err
						project.comments.push
							from: user._id
							field: comment._id
						project.save (err, project)->
							return res.Send err, 400 if err
							return res.send project, 200

	app.get '/project/:id/remove/', (req, res, next)->
		if req.session.email
			User.findOne
				email: req.session.email
			,(err, user)->
				return res.send err, 400 if err
				elem = user.projects.indexOf(req.params.id)
				if elem > -1
					user.projects.splice elem, 1
					user.save (err)->
						return res.send err, 200 if err
					Project.findOne
						_id: req.params.id
					,(err, project)->
						return res.send err, 400 if err
						project.remove()
						return res.send user, 200




