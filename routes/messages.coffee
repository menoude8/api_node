validator = require '../libs/validator'
config = require("../config")
User = require '../models/user'
Project = require '../models/project'
Conversation = require '../models/conversation'

module.exports = (app) ->

	app.post '/messages', (req, res, next)->
		console.log req.body.to
		console.log req.session.username
		errors = []
		unless req.body.to
			errors.push
		        field: 'To'
		        error: 'Destinataire must be filled'
		        val: req.body.to
		if errors.length
			return res.send errors, 400 

		if req.session.email and req.session.token
			User.findOne
				username: req.body.to
			,(err, user)->
				if err
					return res.send err, 400
				term1 = [req.session.username, req.body.to]
				term2 = [req.body.to, req.session.username]
				Conversation.findOne
					$or:[{member: term1}, {member: term2}]
				,(err, conversation)->
					if conversation
						console.log "conversation exist"
						conversation.messages.push
							author: req.session.username
							avatar: req.session.avatar
							body: req.body.message
						conversation.save (err)->
							if err
								return res.send err, 404
							return res.send conversation, 200
					else
						console.log "conversation does not exist creating new one"
						new_conversation = new Conversation
						new_conversation.avatar1 = req.session.avatar
						new_conversation.avatar2 = user.avatar
						new_conversation.member.push req.session.username
						new_conversation.member.push user.username
						new_conversation.messages.push
							author: req.session.username
							avatar: req.session.avatar
							body: req.body.message
						new_conversation.save (err)->
							if err
								return res.send err, 404
							return res.send new_conversation, 200

		else
			return res.send "No session available", 400

	app.get '/api/conversation', (req, res, next)->
		if req.session.email and req.session.token
			Conversation.where('member').in([req.session.username])
			.exec (err, conversation)->
				return res.send conversation, 200 if conversation
				
	app.get '/conversation/:to', (req, res, next)->
		 if req.session.email and req.session.token
		 	term1 = [req.session.username, req.params.to]
		 	term2 = [req.params.to, req.session.username]
		 	Conversation.findOne
		 		$or:[{member: term1}, {member: term2}]
		 	,(err, conversation)->
		 		if conversation
		 			return res.send conversation, 200
		 		else
		 			return res.send err, 400

