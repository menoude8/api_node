module.exports = (io) ->
	usernames = {}
	userids = []
	io.on 'connection', (socket)->
		socket.on 'addUser', (user, room)->
			socket.username = user.username
			socket.room =  room
			socket.avatar = user.avatar
			socket.userid = user.id
			usernames[user.username] = user.username
			if userids.indexOf(user.id) is -1
				userids.push user.id
			socket.join room
			io.to(room).emit('NewUser', "#{user.username} with #{socket.userid } id has join this room", socket.userid, userids)
		socket.on 'Chat', (data)->
			io.to(socket.room).emit('UpdateChat', "#{socket.username} Poke you")
		socket.on 'sendChat', (data)->
			Usersinfos = 
				username: socket.username
				avatar: socket.avatar
				message: data
			io.to(socket.room).emit('newChat', Usersinfos)
		socket.on 'disconnect', ()->
			i = userids.indexOf(socket.userid)
			delete userids[i]
			io.to(socket.room).emit('UpdateRoom', socket.userid)
			socket.leave socket.room