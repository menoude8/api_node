validator = require '../libs/validator'
config = require("../config")
User = require '../models/user'
Project = require '../models/project'
Comment = require '../models/comment'
Task = require '../models/task'
mailer = require '../libs/emailer'

module.exports = (app) ->
	app.post '/room/members', (req, res, next)->
		if req.session.email
			User.findOne
				_id: req.body.member
			,(err, user)->
				return res.send err, 400 if err
				Project.findOne
					_id: req.body.roomref
				,(err, project)->
					return res.send err, 400 if err
					project.room.members.push user._id
					user.invitations.push project._id
					user.save (err, user)->
						return res.send err, 400 if err
					project.save (err)->
						return res.send err, 400 if err
					return res.send user, 200
		else
			return res.send "error session", 400

	app.get '/members/room/:projectref', (req, res, next)->
		if req.session.email
			Project.findOne(
			 _id: req.params.projectref
			).populate('room.members')
			.populate('room.to_do')
			.populate('room.doing')
			.populate('room.done')
			.exec (err, project)->
				return res.send err, 400 if err
				return res.send project, 200

	app.post '/room/task', (req, res, next)->
		if req.session.email
			Project.findOne
				_id: req.body.roomref
			,(err, project)->
				return res.send err, 400 if err
				if req.body.type is "to_do"
					task = new Task
						_creator: req.body.creator
						content: req.body.value
					task.save (err, task)->
						return res.send err, 400 if err
						project.room.to_do.push task._id
						project.save (err, project)->
							return res.send err, 400 if err
							return res.send task, 200
				else if req.body.type is "doing"
					task = new Task
						_creator: req.body.creator
						content: req.body.value
					task.save (err, task)->
						return res.send err, 400 if err
						project.room.doing.push task._id
						project.save (err, project)->
							return res.send err, 400 if err
							return res.send task, 200
				else if req.body.type is "done"
					task = new Task
						_creator: req.body.creator
						content: req.body.value
					task.save (err, task)->
						return res.send err, 400 if err
						project.room.done.push task._id
						project.save (err, project)->
							return res.send err, 400 if err
							return res.send task, 200
		else
			return res.send "Session has been canceled", 400


	app.get '/room/task/:id', (req, res, next)->
		if req.session.email
			Task.findOne(
				_id: req.params.id
			).populate('comments.from')
			.populate('comments.field')
			.exec (err, task)->
				return res.send err, 400 if err
				return res.send task, 200
		else
			return res.send "Session has been canceled", 400

	app.post '/room/task/description', (req, res, next)->
		if req.session.email
			Task.findOne
				_id: req.body.taskref
			,(err, task)->
				return res.send err, 400 if err
				task.description = req.body.description
				task.save (err, task)->
					return res.send err, 400 if err
					return res.send task, 200
		else
			return res.send "Session has been canceled", 400

	app.get '/room/task/remove/:id', (req, res, next)->
		if req.session.email
			Task.findOne
				_id: req.params.id
			,(err, task)->
				return res.send err, 400 if err
				task.remove()
				return res.send "Task removed", 200
	app.post '/room/task/comment', (req, res, next)->
		if req.session.email
			Task.findOne
				_id: req.body.taskid
			,(err, task)->
				return res.send err, 400 if err
				comment = new Comment
						type: 'Task'
						message: req.body.comment
				comment.save (err, comment)->
					return res.send err, 400 if err
					task.comments.push
						from: req.body.userid
						field: comment._id
					task.save (err, task)->
						return res.send err, 400 if err
						return res.send task, 200

