validator = require '../libs/validator'
config = require("../config")
User = require '../models/user'
Project = require '../models/project'

module.exports = (app)->
	app.post '/settings/accounts', (req, res, callback) ->
		if req.session.email
			User.findOne
				email: req.session.email
			,(err, user)->
				user.username = req.body.username
				user.working_at = req.body.working_at
				user.Bio = req.body.bio
				user.contact = req.body.contact
				user.website = req.body.website
				user.master_domain = req.body.master_domain
				user.save (err, user)->
					if user
						return res.send user, 200
					else
						return res.send err, 400

	app.post '/uploads/picture', (req, res, callback) ->
		if req.session.email
			console.log req.files
			User.findOne
				email: req.session.email
			,(err, user)->
				if req.files.userPhoto
					user.avatar = req.files.userPhoto.path
				if req.files.userCover
					user.cover = req.files.userCover.path
				
				user.save (err, user)->
					if err
						return res.send err, 400
					return res.render "index",
						title: "3project"
		else
			return res.send "error", 400