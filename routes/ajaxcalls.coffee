validator = require '../libs/validator'
config = require("../config")
User = require '../models/user'
Project = require '../models/project'
#Notification = require '../models/notification'
module.exports = (app) ->

	Array::contains = (k)->
		i = 0
		while i < @length
			return true if this[i] is k
			i++
		false

	app.get '/userinit', (req, res, next)->
		if req.session.email
			User.findOne(
				email: req.session.email
			).populate('followers')
			.populate('followings') 
			.populate('consulters')
			.populate('activity.projectref')
			.populate('projects')
			.populate('invitations')
			.exec (err, user)->
				if user
					return res.send user, 200
				else
					return res.send "undefined", 400
		else if req.cookies[config.session.cookie.name].email
			User.findOne
				email: req.cookies[config.session.cookie.name].email
			,(err, user)->
				if user
					return res.send user, 200
				else
					return res.send "undefined", 400
	app.get '/user', (req, res, next) ->
		return res.send "error", 400

	app.get '/uploadcover', (req, res, next)->
		console.log "uploadcover"


## search list box server handler
	app.get '/searchbox/:userid', (req, res, next)->
		User.find(
			username: new RegExp(req.params.userid, "i")
		).populate('followers')
		.exec (err, users)->
			if users
				return res.send users, 200
			else
				return res.send err, 400

	app.get '/:userid', (req, res, next)->
		User.find(
			username: new RegExp(req.params.userid, "i")
		).populate('followers')
		.populate('projects')
		.exec (err, users)->
			if users
				return res.send users, 200
			else
				return res.send handleError err, 400

	app.get '/addfollower/:follower', (req, res, next)->

		User.findOne
			username: req.params.follower
		,(err, usertofollow)->
			if err
				return res.send err, 400
			User.findOne
				email: req.session.email
			,(err, thefollower)->
				if err
					return res.send err, 400
				usertofollow.followers.push(thefollower._id)
				usertofollow.friends = []
				thefollower.friends = []
				thefollower.followings.push(usertofollow._id)
				usertofollow.save (err)->
					if err
						return res.send err, 400
				thefollower.save (err)->
					if err
						return res.send err, 400
				return res.send usertofollow, 200

	app.get '/consult/historic/:consultedid', (req, res, next)->
		User.findOne
			username: req.params.consultedid
		,(err, user)->
			if err
				return res.send err, 400
			User.findOne
				email: req.session.email
			,(err, currentuser)->
				if err
					return res.send err, 404
				user.consulters = []
				user.consulters.push(currentuser._id)
				user.save (err)->
					if err
						return res.send err, 404
					return res.send user, 200
					
	app.get '/api/welcome/', (req, res, next)->
		if req.session.email
			User.findOne
				email: req.session.email
			,(err, user)->
				return res.send err, 400 if err
				User.where('followers').in([user._id])
				.populate('projects')
				.exec (err, users)->
					return res.send users, 200 if users
					return res.send err, 400
	app.get '/api/recents', (req, res, next)->
		if req.session.email
			User.findOne
				email: req.session.email
			,(err, user)->
				Project.find(
					_creator: $in: user.followers
				).sort(date: -1)
				.populate('_creator')
				.populate('comments.from')
				.populate('comments.field')
				.exec (err, projects)->
					return res.send projects, 200 if projects
					return res.send err, 400

	app.get '/api/projects/', (req, res, next)->
		if req.session.email
			Project.find(
			).sort(date:-1)
			.limit(10)
			.populate('_creator')
			.populate('comments.from')
			.populate('comments.field')
			.exec (err, projects)->
				return res.send err, 400 if err
				return res.send projects, 200

	app.get '/api/logout/', (req, res, next)->
		if req.session.email
			res.clearCookie('3project_session')
			req.session.destroy()
			res.redirect("/")
			return res.send "ok", 200
		return res.send "err", 404

	app.get '/my/friends', (req, res, next)->
		if req.session.email
			console.log ("ok")
	app.get '/api/suggestion/:param', (req, res, next)->
		if req.session.email
			slug = req.params.param
			Project.find(
				tags: $in: [slug.toUpperCase()]
			).limit(3)
			.sort(date: -1)
			.populate('_creator')
			.exec (err, projects)->
				return res.send "NO EERRROR", 400 if err
				return res.send projects, 200

