config = require '../config'

module.exports = (app) ->
  require('./user')(app)
  require('./ajaxcalls')(app)
  require('./project')(app)
  require('./setting')(app)
  require('./messages')(app)
  require('./logout')(app)
  require('./room')(app)
  home = (req, res, next) ->
    if req.session.email and req.session.token
      console.log "session avalaible"
      res.render 'index', 
      	title: '3project'
      	email: req.session.email
      	token: req.session.token
    else if req.cookies[config.session.cookie.name]
      req.session.email = req.cookies[config.session.cookie.name].email
      req.session.token = req.cookies[config.session.cookie.name].token
      res.render 'index', 
      	title: '3project'
      	email: req.cookies[config.session.cookie.name].email
      	token: req.cookies[config.session.cookie.name].token
    else
      res.render 'login', title: '3project'

  editprofilvalidate = (req, res, next) ->
    if req.session.email and req.session.token
      console.log "session avalaible"
      res.render 'index', 
        title: '3project'
        email: req.session.email
        token: req.session.token
        haschange: "ok"
    else
      res.render 'login', title: '3project'

  app.get "/", home
  app.get "/me/dashboard", home
  app.get "/project/:id", home
  app.get "/settings/account", home
  app.get "/me/timeline", home
  app.get "/settings/account", home
  app.get "/settings/picture", home
  app.get "/gest/:user", home
  app.get "/:id/room", home
  app.get "/get/messages", home
  app.get "/new/project", home
  app.get "/editproject/:id", home
  app.get "/get/newsfeed", home
  app.get "get/newsfeed/:param", home
  app.get "/projecttags/:tags", home
  app.get "/getproject/:param", home
  app.get "/my/friend", home

