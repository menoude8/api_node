validator = require '../libs/validator'
config = require("../config")
User = require '../models/user'
bcrypt = require 'bcrypt'
mailer = require '../libs/emailer'

module.exports = (app) ->

  app.post '/login', (req, res, callback) ->
    errors = []
    unless req.body.email
      errors.push
        field: 'email'
        error: 'Email must be filled'
        val: req.body.email
    else unless validator.isEmail req.body.email
      errors.push
        field: 'email'
        error: 'Email must be a valid email'
    unless req.body.password
      console.log "pass null"
      errors.push
        field: 'password'
        error: 'Password must be filled'
    return res.send errors, 400 if errors.length

    User.findOne
      email: req.body.email
    ,(err, user) ->
      if user
        user.comparePassword req.body.password, (err, isMatch)->
          if isMatch is true
            if req.body.remember is 'true'
              console.log "creating cookies"
              cookievalue = 
                email: user.email
                token: user.token
                id: user._id
                avatar: user.avatar
              res.cookie config.session.cookie.name, cookievalue,
                expires: config.session.cookie.expire()
                path: config.session.cookie.path
                httpOnly: yes
              console.log "cookies info sent"
            req.session.email = user.email
            req.session.token = user.token
            req.session.id = user._id
            req.session.avatar = user.avatar
            req.session.username = user.username
            res.send "", 200
          else
            console.log "Bad Email/Password combinaison"
            return res.send message: "Bad Email/Password combinaison", 400
      else
        console.log "User dont exist"
        return res.send message: "User doesn't exist", 400
    ###
      post handler, but must be in a different file for more organization
    ###

  app.post '/signup', (req, res, callback) ->
    errors = []
    unless req.body.email
      errors.push
        field: 'email'
        error: 'Email must be filled'
        val: req.body.email
    else unless validator.isEmail req.body.email
      errors.push
        field: 'email'
        error: 'Email must be a valid email'
    unless req.body.password
      errors.push
        field: 'password'
        error: 'Password must be filled'
    else if req.body.password.length < config.misc.user.password.min
      errors.push
        field: 'password'
        error: "Password must be at least #{config.misc.user.password.min} characters"
    if req.body.password isnt req.body.passwordConfirm
      errors.push
        field: 'passwordConfirm'
        error: 'Passwords are not equal'
    else unless req.body.passwordConfirm
      errors.push
        field: 'passwordConfirm'
        error: 'Password confirmation must be filled'
    return res.send errors, 400 if errors.length

    ###
    create session, authentification after all input checked
    ###
    ##req.session.i = "ok"
    User.findOne
      email: req.body.email
    , (err, user) ->
      if user
        return res.send message: "Email already exist", 400
      else
        bcrypt.genSalt 10, (err, salt)->
          bcrypt.hash req.body.password, salt, (err, hash)->
            if err
              console.log err
              next()
            newUser = new User
              email: req.body.email
              password: hash
              token: User.generateToken()
              activation: false
            newUser.save (err, user) ->
              if err
                return res.send message: "An error had been occured", 400
              #set email element
              options = 
                to:
                  email: user.email
                  subject: "Activation"
              data = 
                email: user.email
                token: "#{req.protocol}://#{req.headers.host}/activate/#{user.email}/#{user.token}"
              user.sendEmailActivation options, data
              #end of setting email element
              #user.sendEmailActivation "#{req.protocol}://#{req.headers.host}/activate/#{user.email}/#{user.token}"
              res.send
                message: "User saved check your email"
              , 200

  app.get '/activate/:email/:token', (req, res) ->
    User.findOne
      email: req.params.email
      token: req.params.token
    , (err, user) ->
      if user
        user.activation = true
        user.save()
        return res.render 'activation', 
          status: 'Votre compte a bien été activée Merci'
          title: "3project|Activation"
      else
        return res.render 'activation', 
          status: "Désolé mais nous n'avons pas pu vous identifier"
          title: "3project|Activation"




